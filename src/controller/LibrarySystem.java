package controller;

import java.util.ArrayList;

import model.Book;
import model.Instructor;
import model.Library;
import model.ReferencesBook;
import model.Staff;
import model.Student;

public class LibrarySystem {
	private Library l = new Library();;
	Staff staff = new Staff("Yomost","Narak");
	Book book1 = new Book("Big java",400,"Watson");
	Book book2 = new Book("Data",400,"Watson");
	Book book3 = new Book("Thai",400,"Sonsri");
	ReferencesBook refBook = new ReferencesBook("Math",200,"DEF");
	ReferencesBook refBook2 = new ReferencesBook("Math",200,"ABC");
	Student student = new Student("Pimnipa","Pitaktigul","5610400520");
	Instructor instructor = new Instructor("Usa","Sammapan","D14");
	
	public LibrarySystem(){
		
		
		
		l.addBook(book1);
		l.addBook(book1);
		l.addBook(book1);
		l.addBook(book2);
		l.addBook(book3);
		l.addRefBook(refBook);
		l.addRefBook(refBook2);
		
		System.out.println("Book in Library: "+l.bookCount()+"\n");
		
		//Student can borrowing 7 day.
		System.out.println("***Student borrowing Big java book***");	

		System.out.println("\n>>Borrowing");	
		borrowing(student, "Big java");
		l.borrowBook(student, "Big java");
		System.out.println("Name book: "+student.getBorrowedBook(l));
		System.out.println("Name student: "+student.getName());
		System.out.println("Big java in Library: "+ l.checkBookCount("Big java"));	
		System.out.println("Book in Library: "+l.bookCount());
		System.out.println("Dateline return: "+student.dateLine());
		
		System.out.println("\n>>Return book");
		l.returnBook(student, "Big java");
		System.out.println("Name student return: "+student.getName());
		System.out.println("Name book return: "+student.getBorrowedBook(l));
		System.out.println("Day can borrow: "+l.borrowDay());
		System.out.println("Big java in Library: "+ l.checkBookCount("Big java"));	
		System.out.println("Book in Library: "+l.bookCount()+"\n");

		
		
		System.out.println("***Student borrowing Algorithm book***");	
		System.out.println("\n>>Checking book");
		borrowing(student, "Algorithm");
		
		
		//Staff can borrowing 14 day.
		System.out.println("\n***Staff borrowing Data book***");	
		System.out.println("\n>>Checking book");
		borrowing(staff, "Data");
		
		System.out.println("\n>>Borrowing");
		l.borrowBook(staff, "Data");
		System.out.println("Name book: "+staff.getBorrowedBook(l));
		System.out.println("Name staff: "+staff.getName());
		System.out.println("Data in Library: "+ l.checkBookCount("Data"));	
		System.out.println("Book in Library: "+l.bookCount());
		System.out.println("Dateline return: "+staff.dateLine());
		
		System.out.println("\n>>Return book");
		l.returnBook(staff, "Data");
		System.out.println("Name staff return: "+staff.getName());
		System.out.println("Name book return: "+staff.getBorrowedBook(l));
		System.out.println("Day can borrow: "+l.borrowDay());
		System.out.println("Data in Library: "+ l.checkBookCount("Data"));	
		System.out.println("Book in Library: "+l.bookCount()+"\n");
		
		
		//Instructor can borrowing 10 day and can borrowing referance book 3 day.
		System.out.println("***Instructor borrowing Algo book***");	
		borrowing(instructor, "Algo");
		
		System.out.println("\n***Instructor borrowing Thai book***");	
		borrowing(instructor, "Thai");
		System.out.println("\n>>Borrowing");
		System.out.println("Name book: "+instructor.getBorrowedBook(l));
		l.borrowBook(instructor, "Thai");
		System.out.println("Name staff: "+instructor.getName());
		System.out.println("Thai in Library: "+ l.checkBookCount("Thai"));	
		System.out.println("Book in Library: "+l.bookCount());
		System.out.println("Dateline return: "+instructor.dateLine());
		
		System.out.println("\n>>Return book");
		l.returnBook(instructor, "Thai");
		System.out.println("Name instructor return: "+instructor.getName());
		System.out.println("Name book return: "+instructor.getBorrowedBook(l));
		System.out.println("Day can borrow: "+l.borrowDay());
		System.out.println("Thai in Library: "+ l.checkBookCount("Thai"));	
		System.out.println("Book in Library: "+l.bookCount()+"\n");
		
		
		System.out.println("***Instructor borrowing Math book***");	
		borrowing(instructor, "Math");
		System.out.println("Name book: "+instructor.getBorrowedBook(l));
		l.borrowBook(instructor, "Math");
		l.listRefBook().remove("Math");
		System.out.println("Name staff: "+instructor.getName());
		System.out.println("Dateline return: "+instructor.dateLineRefBook());
		System.out.println("Math in Library: "+ l.checkBookCountRef("Math"));	
		System.out.println("Book in Library: "+l.bookCount());
		
		System.out.println("\n>>Return book");
		l.returnBook(instructor, "Math");
		System.out.println("Name instructor return: "+instructor.getName());
		System.out.println("Name book return: "+instructor.getBorrowedBook(l));
		System.out.println("Day can borrow: "+l.borrowDay());
		System.out.println("Math in Library: "+ l.checkBookCount("Math"));	
		System.out.println("Book in Library: "+l.bookCount()+"\n");

	}

	public static void main(String[] args) {
		new LibrarySystem();
		
	}
	public void borrowing(Student student,String nameBook){	
		if(l.checkBook(nameBook)){
			System.out.println("It have "+nameBook+" "+l.checkBookCount(nameBook)+" book.");
		}
		else if(l.checkRefBook(nameBook)){
			System.out.println("You can't borrowing this book");
		}
		else{
			System.out.println("Don't have "+nameBook+" book.");
		}
	}
	public void borrowing(Staff staff,String nameBook){	
		if(l.checkBook(nameBook)){
			System.out.println("It have "+nameBook+" "+l.checkBookCount(nameBook)+" book.");
		}
		else if(l.checkRefBook(nameBook)){
			System.out.println("You can't borrowing this book");
		}
		else{
			System.out.println("Don't have "+nameBook+" book.");
		}
	}
	public void borrowing(Instructor instructor,String nameBook){		
		if(l.checkRefBook(nameBook)){
			System.out.println("It have "+nameBook+" "+l.checkBookCountRef(nameBook)+" book.It referance book!");
		}
		else if(l.checkBook(nameBook)){
			System.out.println("It have "+nameBook+" "+l.checkBookCount(nameBook)+" book.");	
		}
		else{
			System.out.println("Don't have "+nameBook+" book.");
		}
	}
	
	public void checkDayReturn(Student student,int day){
		if (student.dateLine()<l.borrowDay()){
			System.out.println("Out time borrow "+l.borrowDay()+"day.");
		}
		else{
			System.out.println("time borrow: "+l.borrowDay()+"day.");
		}
	}
	public void checkDayReturn(Staff staff){
		if (staff.dateLine()<l.borrowDay()){
			System.out.println("Out time borrow "+l.borrowDay()+"day.");
		}
		else{
			System.out.println("time borrow: "+l.borrowDay()+"day.");
		} 
	}
	public void checkDayReturn(Instructor instructor){
		if (instructor.dateLine()<l.borrowDay()){
			System.out.println("Out time borrow "+l.borrowDay()+"day.");
		}
		else{
			System.out.println("time borrow: "+l.borrowDay()+"day.");
		}
	}

}
