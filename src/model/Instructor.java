package model;

public class Instructor {
	private String firstName,lastName,departmentId;
	public Instructor(String firstName, String lastName,String departmentId){
		this.firstName = firstName;
		this.lastName = lastName;
		this.departmentId = departmentId;
		
	}
	public String getName(){
		return this.firstName +" "+ this.lastName;
		
	}

	public String getBorrowedBook(Library l){
		return l.getNameBook();	
	}
	public int dateLine() {
		return 10;
	}
	public int dateLineRefBook() {
		return 3;
	}
	


}
