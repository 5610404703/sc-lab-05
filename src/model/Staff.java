package model;

public class Staff {
	private String firstName,lastName;
	public Staff(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
		
	}
	public String getName(){
		return this.firstName + " " +this.lastName;
		
	}

	public String getBorrowedBook(Library l){
		return l.getNameBook();	
	}
	public int dateLine() {
		return 14;
	}


}
