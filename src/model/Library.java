package model;

import java.util.ArrayList;

public class Library {
	private int n, day;
	private String name,nameBook;
	private ArrayList<String> dataBook = new ArrayList<String>();
	private	ArrayList<String> dataRefBook = new ArrayList<String>();
	
	public void addBook(Book book){	
		 dataBook.add(book.getName());
		 n ++;
	}
	
	public void addRefBook(ReferencesBook refBook){
		dataRefBook.add(refBook.getName());
		n ++;	
	}
	
	
	public int bookCount(){		
		return n;
	}
	
	public ArrayList<String> listBook(){
		return dataBook;
	}
	
	public ArrayList<String> listRefBook(){
		return dataRefBook;
	}
	
	public boolean checkBook(String nameBook){
		if (listBook().contains(nameBook)){
			return true;
		}
		else{
			return false;
		}
	}
	public boolean checkRefBook(String nameBook) {
		if (listRefBook().contains(nameBook)){
			return true;
		}
		else{
			return false;
		}		
	}
	
	public int checkBookCount(String nameBook){
		int count=0;
		for(int i = 0; i<listBook().size(); i++){
			if (nameBook==listBook().get(i)){
				count += 1;
			}					
		}
		return count;
	}
	public int checkBookCountRef(String nameBook){
		int count=0;
		for(int i = 0; i<listRefBook().size(); i++){
			if (nameBook==listRefBook().get(i)){
				count += 1;
			}					
	}
	return count;
	}
	
	
	public void borrowBook(Student student,String string){
		name = student.getName();
		nameBook = string;
		listBook().remove(nameBook);
		n --;	
	}
	
	public void borrowBook(Staff staff,String string){
		name = staff.getName();
		nameBook = string;
		listBook().remove(nameBook);
		n --;		
	}
	
	public void borrowBook(Instructor instructor,String string){
		name = instructor.getName();
		nameBook = string;
		listBook().remove(nameBook);
		n --;	
	}
	
	public void returnBook(Student student,String string){
		name = student.getName();
		nameBook = string;
		listBook().add(nameBook);
		n ++;	
	}
	
	public void returnBook(Staff staff,String string){
		name = staff.getName();
		nameBook = string;
		listBook().add(nameBook);
		n ++;		
	}
	
	public void returnBook(Instructor instructor,String string){
		name = instructor.getName();
		nameBook = string;
		listBook().add(nameBook);
		n ++;	
	}
	
	public int borrowDay(){
		//�ӹǳ���ҷ�����Ẻ�Ѻ�����ѧ
		day = 2;
		return day;
	}
	
	public String getBorrowers() {	
		return name;
	}
	
	public String getNameBook() {
		return nameBook;
	}


}
