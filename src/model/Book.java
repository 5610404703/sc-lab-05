package model;

import java.util.ArrayList;

public class Book {
	private String name,author;
	private double page;
	private ArrayList<String> listBook;
	
	public Book(String name,double page,String author){
		this.name = name;
		this.page = page ;
		this.author = author;
	}
	public String getName(){
		return name;	
	}
}
