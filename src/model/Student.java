package model;

public class Student {
	private String firstName,lastName,id;
	
	public Student(String firstName, String lastName,String id){
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
	}
	public String getName(){
		return this.firstName + " "+this.lastName;
		
	}
	public String getBorrowedBook(Library l){
		return l.getNameBook();	
	}
	public int dateLine() {
		return 7;
	}
	
	

}
